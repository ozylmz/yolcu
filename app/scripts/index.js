var app = angular.module('myApp', []);

app.controller('customersCtrl', function ($scope, $http,$filter) {


    $scope.dataUrl = "sources/data.json";
    var orderCategory = [];
    $scope.data = [];

    $scope.getData = function (){
        $http({
            method: 'GET',
            url: $scope.dataUrl
        }).then(function (response) {
            var datas = response.data;
            datas = $filter('orderBy')(datas, 'name', false);

            angular.forEach(datas, function (data) {
                if (typeof data.parentId === "undefined") {
                    checkFunction(data.id);
                    orderCategory[data.id]['main'] = data;
                }
                else {
                    checkFunction(data.parentId);

                    if (typeof orderCategory[data.parentId]['childs'] === "undefined") {
                        orderCategory[data.parentId]['childs'] = [];
                    }


                    orderCategory[data.parentId]['childs'].push(data)
                }
            }, orderCategory);

            //Fix keys
            // angular.forEach(orderCategory, function(data) {
            //     $scope.data.push(data);
            // }, $scope.data);
            $scope.data = orderCategory;
            $scope.selectedData = $scope.data[1];

        }, function (error) {

        });
    };

    $scope.deleteChildItem = function (index) {
        console.log(index);
        var parentid = $scope.selectedData.main.id;
        // delete $scope.data[parentid]['childs'][index];
        $scope.data[parentid]['childs'].splice(index, 1);
    };

    $scope.deleteMainItem = function (index) {
        var parentid = $scope.selectedData.main.id;
        console.log(parentid);
        // $scope.data.splice(parentid, 1);
        delete $scope.data[parentid];
        console.log($scope.data);

        //Select First Element Of Datas
        for(var key in $scope.data) break;

        console.log(key);
        console.log($scope.selectedData);
        $scope.selectedData = $scope.data[key];
        console.log($scope.selectedData);


    };

    var checkFunction = function (index) {
        if (typeof orderCategory[index] === "undefined") {
            orderCategory[index] = [];
        }
    };

    $scope.getData();


});